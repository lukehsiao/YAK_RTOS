/********************************************************************
 * Luke Hsiao & Tyler Bellows
 * ECEn 425 | RTOS
 * 19 Nov 2014
 * 
 * This contains functionality for priority queues
 *******************************************************************/
/*
Since these functions can be called from semaphores, queues, or events, we should also pass in the head of the list
*/


// Initializes a queue
void InitializePriorityQueue(PriorityQueue* queue) {
    queue->size = 0;
    queue->head = NULL;
    queue->tail = NULL;
}


/*
Return the new head of the list?
*/
TCBptr PriorityQueueRemove(TCBptr itemToRemove, TCBptr headOfList){
	TCBptr newHeadOfList, tmp1;	

	if(itemToRemove == headOfList{//itemToRemove is the head of list
		newHeadOfList = itemToRemove->next;
		newHeadOfList->prev = NULL;
		newHeadOfList->next = itemToRemove->next->next;
		return newHeadOfList;
	}
	else if(itemToRemove->next == NULL){ //itemToRemove is the tail of list
		newHeadOfList = headOfList;
		tmp1 = itemToRemove->prev;
		tmp1->prev = itemToRemove->prev->prev;
		tmp1->next = NULL;
		return newHeadOfList;
		
	}
	else{ //itemToRemove is in the middle of list
		newHeadOfList = headOfList;
		tmp1 = itemToRemove->prev;
		tmp1->prev = itemToRemove->prev->prev;
		tmp1->next = itemToRemove->next;
		return newHeadOfList;
	}
}

 
/*
Do we need to return the head of the list?
*/
void PriorityQueueAdd(TCBptr itemToAdd, TCBptr headOfList){
	TCBptr tmp1, tmp2;
	
	if(headOfList == NULL)
	{
	    headOfList = itemToAdd;
            itemToAdd->next = NULL;
            itemToAdd->prev = NULL;
	}
	else
	{
            //tmp2 = queue->waitList; // head of the current waitList
	tmp2 = headOfList;            

            // Moves tmp2 to item to insert below
            while (tmp2->priority < itemToAdd->priority) {
                if (tmp2->next != NULL) {
                    //If the following item is lower priority
                    if (tmp2->next->priority > itemToAdd->priority) {
                        break;
                    }
                    else {
                        tmp2 = tmp2->next;
                    }                    
                }
                else {
                    break;  //jump out of the loop
                }
            }
            // Actually insert the item
            if (tmp2->next == NULL) {
                tmp2->next = tmp1;
                itemToAdd->prev = tmp2;
                itemToAdd->next = NULL;
            }
            else {
                tmp2->next->prev = tmp1;
                tmp1->next = tmp2->next;
                tmp2->next = tmp1;
                tmp1->prev = tmp2;                
            }
	}

	
}
