/********************************************************************
 * Luke Hsiao & Tyler Bellows
 * ECEn 425 | Lab 4A
 * 30 Sept 2014
 * 
 * The values define here MUST be modified by the user for correct
 * operation!
 *
 *******************************************************************/

// change this to fit needs
#define IDLE_STACK_SIZE 256    

// Max Number of user tasks
#define MAX_TCBs 6

// Max Number of semaphores used by the user
#define MAX_SEMs 5 //

// Max number of queues used by the user
#define MAX_QUEUEs 5

// Max number of events
#define MAX_EVENTs 3
