# YAK Kernel #

This is a simple but fully-functional 8086 RTOS that is preemptive and features semaphores, message queues, events, and interrupts.

### Contribution guidelines ###

When doing development, please branch and develop there, then submit a pull request into our stable master branch.

### Who do I talk to? ###

Luke Hsiao & Tyler Bellows
