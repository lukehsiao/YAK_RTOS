/********************************************************************
 * Luke Hsiao & Tyler Bellows
 * ECEn 425 | Lab 4A
 * 30 Sept 2014
 * 
 * This contains the kernel routines written in C.  Global variables
 * used by kernel code or shared by kernel and app code should also
 * be defined in this file.
 *******************************************************************/
 
 #include "yakk.h"
 #include "clib.h"
 #define DEBUG      // Comment this out to remove dumpList() from being loaded
 
 //*********************** Global Variables **************************
u16 YKIdleCount;            // increments each loop of the idle task
u16 YKCtxSwCount;           // incremented each time a context switch occurs
u16 YKTickNum;              // incremented when the kernel's tick handler runs
u16 YKISRDepth;             // tracks how deep in nesting we go
u8  runEnabled;             // tracks whether YKRun() has been called
TCBptr YKRdyList;           //List of TCBS of ready task (high->low priority)
TCBptr YKSuspList;          //Tasks delayed/suspended (ranked low->high delay)
TCBptr YKAvailList;         //List of all available TCBs
TCBptr YKCurrentTask;       //pointer to the currently running task
TCB YKTCBArray[MAX_TCBs+1]; // +1 for the idle task

YKSEM YKSEMArray[MAX_SEMs];
u16 availSemIndex;          // index of the next available semaphore

YKQ YKQArray[MAX_QUEUEs];
u16 availQueueIndex;        // index of the next available queue

YKEVENT YKEVENTArray[MAX_EVENTs];
u16 availEventIndex;

int IdleStk[IDLE_STACK_SIZE];

//====================== End Global Variables =======================


 /**
 * Initializes all required kernel data structures. This must be called
 * from main() in the application code before any other YAK functions.
 * It must create YKIdleTask and is responsible for allocating its
 * stack space according to a #define.
 *
 */
void YKInitialize(void) {    
    //ALWAYS DECLARE VARIABLES FIRST
    int i;
    TCBptr tmp1, tmp2;
    SEMptr sem1, sem2;
    YKEnterMutex();
    //Create the singly-linked available TCB list from initial array
    YKAvailList = &(YKTCBArray[0]); //assigned address of the memory
    for (i = 0; i < MAX_TCBs; i++) {
        YKTCBArray[i].next = &(YKTCBArray[i+1]);
    }
    YKTCBArray[MAX_TCBs].next = NULL;    
    
    availSemIndex = 0;
    availQueueIndex = 0;
    availEventIndex = 0;

    //Initialize NULL Lists
    YKRdyList = NULL;
    YKSuspList = NULL;
    
    //Initialize Counters
    YKISRDepth = 0;
    YKTickNum = 0;
    YKCtxSwCount = 0;
    YKIdleCount = 0;
    runEnabled = 0;
    
    // Create new TCB for IdleTask and add to Ready List
    // Set the IdleTask as the lowest priority
    YKNewTask(YKIdleTask, (void *)&IdleStk[IDLE_STACK_SIZE], 100); 
    
}



/**
 * This is the kernel's idle task, which is always the lowest
 * priority task in the system. It just spins in while(1).
 * This is only referenced by kernel code and is never
 * explicitly called. 
 * It also increments the global variable YKIdleCount atomically.
 *
 */
void YKIdleTask(void) {
    u16 nothing;
    // For this lab, we assume a tick rate of 10,000/tick
    // and report CPU utilization every 20 ticks.
    // This requires the while(1) to take 4 instructions to execute
    // so that YKIdleCount does not overflow.
    while(1) {
        YKEnterMutex();
        nothing++;
        YKIdleCount++;
        YKExitMutex();
    }
}

/**
 * Registers a task with the kernel, causing the allocation and
 * initialization of whatever data structures the kernel uses to
 * represent tasks.  Must be called exactly once for every task
 * in the system. Note *taskStack should be one word BEYOND the top of
 * stack. This can never be run by an interrupt handler.
 *
 * MUST BE REENTRANT
 *
 * @param (*task)(void) function pointer to the C function of task code
 * @param *taskStack pointer to the top of the stack space reserved for task
 * @param priority A priority value from [0,100]. Lower # = higher priority.
 */
void YKNewTask(void(*task)(void), void *taskStack, u8 priority) {
    TCBptr tmp1, tmp2;
    u16* stackPointer;
    stackPointer = (u16 *) taskStack;
    YKEnterMutex();

    // Setup New Task's stack for first run

    stackPointer--; *stackPointer = FLAG_DEFAULT;   // Flags with Interrupts ON
    stackPointer--; *stackPointer = 0;              // CS
    stackPointer--; *stackPointer = (u16) task;     // IP
    stackPointer--; *stackPointer = 0;              // AX
    stackPointer--; *stackPointer = 0;              // BX
    stackPointer--; *stackPointer = 0;              // CX
    stackPointer--; *stackPointer = 0;              // DX
    stackPointer--; *stackPointer = 0;              // SI
    stackPointer--; *stackPointer = 0;              // DI
    stackPointer--; *stackPointer = 0;              // BP
    stackPointer--; *stackPointer = 0;              // ES
    stackPointer--; *stackPointer = 0;              // DS

    
    // Take first unused TCB from the available list
    tmp1 = YKAvailList;
    YKAvailList = tmp1->next;
    
    // Set tmp1's properties
    tmp1->sp = (void*)stackPointer;
    tmp1->state = READY;
    tmp1->delayTicks = 0;
    tmp1->priority = priority;
    tmp1->eventMask = 0;
    tmp1->waitMode = 0;
    
    // Add this first entry to the doubly-linked ready list sorted
    // by priority
    if (YKRdyList == NULL) {
        YKCurrentTask = tmp1;
        YKRdyList = YKCurrentTask;
        YKRdyList->next = NULL;
        YKRdyList->prev = NULL;
    }
    // For all the other insertions, insert sorted by priority
    else {
        if (runEnabled == 1) {
            YKCurrentTask->state = PAUSED;
        }
        tmp2 = YKRdyList;
        while (tmp2->priority < tmp1->priority) {
            tmp2 = tmp2->next; //assumes idle task is at the end
        }
        if (tmp2->prev == NULL) {
            YKRdyList = tmp1;
        }
        else {
            tmp2->prev->next = tmp1;
        }
        tmp1->prev = tmp2->prev;
        tmp1->next = tmp2;
        tmp2->prev = tmp1;
    }
    if (runEnabled == 1) {
        YKScheduler();
    }
}

/**
 * Called from main() in the application code and never returns.
 * It tells the kernel to begin execution of the tasks in the application
 * code.  It marks the transition from initial setup to actual execution.
 * At least one user-defined task must be created before this is called.
 */
void YKRun(void) {
    runEnabled = 1;
    YKScheduler();
}

/**
 * Delays a task for the specified number of clock ticks.  After 
 * saving context, etc, this calls the scheduler.  After the specified
 * number of ticks, the kernel marks the task as ready.
 * If called with a count of 0, it does not delay and simply returns.
 * This is only called by tasks, never by interrupt handlers or ISRs.
 *
 * MUST BE REENTRANT
 */
void YKDelayTask(u16 count) {

   TCBptr tmp;
    
    if (count == 0) {
        return;
    }    
    YKEnterMutex();
     
    /* code to remove an entry from the ready list and put in
   suspended list, which is not sorted.  (This only works for the
   current task, so the TCB of the task to be suspended is assumed
   to be the first entry in the ready list.)   */
    YKCurrentTask->state = BLOCKED;
    YKCurrentTask->delayTicks = count;    
    tmp = YKRdyList;		/* get ptr to TCB to change */
    YKRdyList = tmp->next;	/* remove from ready list */
    tmp->next->prev = NULL;	/* ready list is never empty */
    tmp->next = YKSuspList;	/* put at head of delayed list */
    YKSuspList = tmp;
    tmp->prev = NULL;
    if (tmp->next != NULL) {	/* susp list may be empty */
	    tmp->next->prev = tmp;
	}
	YKCurrentTask = tmp;
    if (runEnabled == 1) {
        YKScheduler();
    }
    YKExitMutex(); 
}


/**
 * Must be called from the tick ISR each time it runs. Responsible for
 * the bookkeeping required to support timely reawakening of delayed tasks
 * The tick ISR may also call a user tick handler if user code requires
 * actions to be taken on each clock tick.
 */
void YKTickHandler(void) {
    TCBptr tmp, tmp2, tmp3;
    u16 tempDelay;
    YKTickNum++;
    // Code to remove an entry from the suspended list and insert it in the
    // sorted ready list.  tmp points to the TCB to be moved.
    tmp = YKSuspList;
    tmp3 = YKSuspList;
    while (tmp != NULL) {     // Iterate through all suspended tasks
        YKEnterMutex();
        tempDelay = tmp->delayTicks;
        tmp->delayTicks = (tempDelay-1);
        tmp3 = tmp->next;   //save next element
        if (tmp->delayTicks == 0) {
            tmp->state = READY;
            //Move to the ready 
            if (tmp->prev == NULL) {	/* fix up suspended list */
                YKSuspList = tmp->next;
            }
            else {
                tmp->prev->next = tmp->next;
            }
            
            if (tmp->next != NULL) {
                tmp->next->prev = tmp->prev;
            }
            tmp2 = YKRdyList;  /* put in ready list (idle task always at end) */
            while (tmp2->priority < tmp->priority) {
                tmp2 = tmp2->next;
            }
            if (tmp2->prev == NULL)	{  /* insert before TCB pointed to by tmp2 */
                YKRdyList = tmp;
            }
            else {
                tmp2->prev->next = tmp;
            }
            tmp->prev = tmp2->prev;            
            tmp->next = tmp2;
            tmp2->prev = tmp;
        }
        // Advance to next element of YKSuspList
        tmp = tmp3;
        YKExitMutex();
    }
    
}

/**
 * This function must be called near the beginning of each ISR just before
 * interrupts are re-enabled.  This simply increments YKISRDepth
 */
void YKEnterISR(void) {
    YKISRDepth++;
    YKCurrentTask->state = INTERRUPTED;
}

/**
 * This function must be called near the end of each ISR after interrupts are
 * disabled.  This simply increments YKISRDepth and calls the scheduler
 */
void YKExitISR(void) {
    YKISRDepth--;
    if (runEnabled == 1 && YKISRDepth == 0) {
        YKScheduler();
    }
}

/**
 * Determines the highest priority task, then calls the dispatcher
 * to cause that task to execute.
 */
void YKScheduler(void) {
    u8 tempState, headState;
    YKEnterMutex();
    tempState = YKCurrentTask->state;   // INTERRUPTED, BLOCKED, or PAUSED
    // If the current task's priority is lower than the head of RdyList
    if((YKCurrentTask->priority > YKRdyList->priority) || tempState == BLOCKED){
	    YKCtxSwCount++; //increment context switch counter
	    if (tempState == PAUSED || tempState == INTERRUPTED) {
            YKCurrentTask->state = READY;   // if it's still on ready list
        }
	    //YKCurrentTask = YKRdyList;
	    //YKCurrentTask->state = RUNNING;
	    if (tempState == BLOCKED || tempState == PAUSED) {
	        YKDispatcher(SAVE_CONTEXT); 
        }
        else {
            YKDispatcher(DONT_SAVE_CONTEXT);
        }
    }   
}

/**
 * This function creates and initializes a semaphore and must be called exactly
 * once per semaphore. This call is typically in main in the user code. The
 * initialization value must be non-negative. The value returned is a pointer
 * to the data structure used by the kernel to represent the semaphore.
 * YKSEM is a typedef defined in a kernel header file that must be included
 * in any user file that uses semaphores.  Despite having a pointer to this
 * struct, user code never needs to know implementation details of semaphores.
 */
YKSEM* YKSemCreate(int initialValue) {
    YKSEM* tmp1;
    YKEnterMutex();
    // Take first unused SEM from the array
    if (availSemIndex == MAX_SEMs) {
        printString("\n\n!!!!!! Error, created too many semaphores !!!!!\n\n");
    }
    else {
        tmp1 = &YKSEMArray[availSemIndex];
        availSemIndex++;
    }
    
    // Set tmp1's properties
    tmp1->value = initialValue;
    tmp1->waitList = NULL;
    YKExitMutex();
    return tmp1;
}

/**
 * This function tests the value of the indicated semaphore then decrements it.
 * If the value before decrementing was greater than zero, the code returns to
 * the caller. If the value before decrementing was less than or equal to zero,
 * the calling task is suspended by the kernel until the semaphore is available
 * and the scheduler is called. This function is called only by tasks, never by
 * ISRs or interrupt handlers.
 */
void YKSemPend(YKSEM* semaphore) {
    TCBptr tmp1, tmp2;
    YKEnterMutex();
    // If semaphore is available, return
    if (semaphore->value > 0) {
        (semaphore->value)--;
        YKExitMutex();
        return;        
    }
    // Otherwise, BLOCK
    else {
        
        (semaphore->value)--;
        //Remove from the Ready List and place on the semaphore's blocked list
        YKCurrentTask->state = BLOCKED;
        YKCurrentTask->delayTicks = 0;  //doesn't matter    
        tmp1 = YKRdyList;		/* get ptr to TCB to change */
        YKRdyList = tmp1->next;	/* remove from ready list */
        tmp1->next->prev = NULL;	/* ready list is never empty */
        
        if (semaphore->waitList == NULL) {
            semaphore->waitList = tmp1;
            tmp1->next = NULL;
            tmp1->prev = NULL;
        }
        // For all the other insertions, insert sorted by priority
        else {
            tmp2 = semaphore->waitList;
            
            // Moves tmp2 to item to insert below
            while (tmp2->priority < tmp1->priority) {
                if (tmp2->next != NULL) {
                    //If the following item is lower priority
                    if (tmp2->next->priority > tmp1->priority) {
                        break;
                    }
                    else {
                        tmp2 = tmp2->next;
                    }                    
                }
                else {
                    break;  //jump out of the loop
                }
            }
            if (tmp2->next == NULL) {
                tmp2->next = tmp1;
                tmp1->prev = tmp2;
                tmp1->next = NULL;
            }
            else {
                tmp2->next->prev = tmp1;
                tmp1->next = tmp2->next;
                tmp2->next = tmp1;
                tmp1->prev = tmp2;                
            }
        }
        
	    YKCurrentTask = tmp1;
	    if (runEnabled == 1) {
            YKScheduler();
        }
        YKExitMutex();
    }
}

/**
 * This function increments the value of the indicated semaphore. If any
 * suspended tasks are waiting for this semaphore, the waiting task with the
 * highest priority is made ready.  Unlike YKSemPend, this function may be
 * called from both task code and interrupt handlers. If called from task code
 * then the function should call the scheduler. Otherwise, the scheduler isn't
 * called.
 */
void YKSemPost(YKSEM* semaphore) {
    TCBptr tmp1, tmp2;
    YKEnterMutex();
    (semaphore->value)++;
    
    if (semaphore->value > 0) {
        YKExitMutex();
        return;
    }
    
    // Move the highest priority waiting task to ready list
    if (semaphore->waitList != NULL) {
        tmp1 = semaphore->waitList;
        semaphore->waitList = tmp1->next;
        semaphore->waitList->prev = NULL;
        
        tmp1->state = READY;
        tmp2 = YKRdyList; /* put in ready list (idle task always at end) */
        while (tmp2->priority < tmp1->priority) {
            tmp2 = tmp2->next;
        }
        if (tmp2->prev == NULL)	{/* insert before TCB pointed to by tmp2 */
            YKRdyList = tmp1;
        }
        else {
            tmp2->prev->next = tmp1;
        }
        tmp1->prev = tmp2->prev;            
        tmp1->next = tmp2;
        tmp2->prev = tmp1;
    }
    // Only call the scheduler if running from task code
    if (YKISRDepth == 0) {
        YKCurrentTask->state = PAUSED;
        YKScheduler();
    }
    YKExitMutex();
}

/**
 * Initializes data structure used to maintain queue and returns pointer to it.
 * Called once per message queue (ideally in main). 
 */
YKQ *YKQCreate(void **start, u16 length) {
    YKQ* tmp1;
    YKEnterMutex();
    // Take first unused Queue from the array
    if (availQueueIndex == MAX_QUEUEs) {
        printString("\n\n!!!!!! Error, created too many queues !!!!!\n\n");
    }
    else {
        tmp1 = &YKQArray[availQueueIndex];
        availQueueIndex++;
    }
    
    // Set tmp1's properties
    tmp1->baseAddress = start;
    tmp1->waitList = NULL;
    tmp1->length = length;
    tmp1->head = 0;
    tmp1->tail = 0;
    YKExitMutex();
    return tmp1;
}

/** 
 * Removes and returns the oldest message from the message queue if non-empty. 
 * If the message queue is empty, calling task is suspended by the kernel until
 * a message is placed in the queue.
 * Called only by tasks, NEVER by interrupt code.
 * @param queue pointer to the queue
 */
void *YKQPend(YKQ *queue) {
    u16 oldMessage;  // This is a u16 to allow pointer arthmetic
    void* baseAddress;
    TCBptr tmp1, tmp2;
    YKEnterMutex();
    
    // If the queue is empty
    if (queue->head == queue->tail) {
        // Remove from the Ready List and place on the Queue's blocked list
        YKCurrentTask->state = BLOCKED;
        YKCurrentTask->delayTicks = 0;  //doesn't matter    
        tmp1 = YKRdyList;		/* get ptr to TCB to change */
        YKRdyList = tmp1->next;	/* remove from ready list */
        tmp1->next->prev = NULL;	/* ready list is never empty */

        if (queue->waitList == NULL) {
            queue->waitList = tmp1;
            tmp1->next = NULL;
            tmp1->prev = NULL;
        }
        // For all the other insertions, insert sorted by priority
        else {
            tmp2 = queue->waitList; // head of the current waitList
            
            // Moves tmp2 to item to insert below
            while (tmp2->priority < tmp1->priority) {
                if (tmp2->next != NULL) {
                    //If the following item is lower priority
                    if (tmp2->next->priority > tmp1->priority) {
                        break;
                    }
                    else {
                        tmp2 = tmp2->next;
                    }                    
                }
                else {
                    break;  //jump out of the loop
                }
            }
            // Actually insert the item
            if (tmp2->next == NULL) {
                tmp2->next = tmp1;
                tmp1->prev = tmp2;
                tmp1->next = NULL;
            }
            else {
                tmp2->next->prev = tmp1;
                tmp1->next = tmp2->next;
                tmp2->next = tmp1;
                tmp1->prev = tmp2;                
            }
        }

        YKCurrentTask = tmp1;
        if (runEnabled == 1) {
            YKScheduler();
        }
    }
	//if the queue is not empty
	if (queue->head != queue->tail) {
	    baseAddress = queue->baseAddress;
	    oldMessage = ((u16*)baseAddress)[queue->head];
		queue->head = ((queue->head)+1) % queue->length;	
		YKExitMutex();	
		return (void*)oldMessage;
	}
}


/**
 * Inserts a message (pointer) into message queue. If suspended tasks are 
 * waiting for a message from this queue, the highest priority suspended 
 * task is made ready to run. Called from both task and interrupt code. 
 *
 * @param queue = pointer to queue to use
 * @param msg = pointer to message
 * @return 1 if the queue is not full and the message insertion was successful
 */
int YKQPost(YKQ *queue, void *message) {
    TCBptr tmp1, tmp2;
    void* baseAddress;
    YKEnterMutex();
	if (queue->head == ((queue->tail)+1) % queue->length) { //queue is full
	    YKExitMutex();
		return 0;
	}
	// If queue is not full, insert the message
	else {
	    baseAddress = queue->baseAddress;
	    ((u16*)baseAddress)[queue->tail] = (u16)message;
		queue->tail = ((queue->tail)+1) % queue->length;		
		
		// Move highest priority waiting task to ready list
		if (queue->waitList != NULL) {
	        // Remove first item
            tmp1 = queue->waitList;
            queue->waitList = tmp1->next;
            queue->waitList->prev = NULL;
            
            tmp1->state = READY;
            tmp2 = YKRdyList; /* put in ready list (idle task always at end) */
            while (tmp2->priority < tmp1->priority) {
                tmp2 = tmp2->next;
            }
            if (tmp2->prev == NULL)	{/* insert before TCB pointed to by tmp2 */
                YKRdyList = tmp1;
            }
            else {
                tmp2->prev->next = tmp1;
            }
            tmp1->prev = tmp2->prev;            
            tmp1->next = tmp2;
            tmp2->prev = tmp1;
        }
        // Only call the scheduler if running from task code
        if (YKISRDepth == 0) {
            YKCurrentTask->state = PAUSED;
            YKScheduler();
        }
		YKExitMutex();
		return 1;
	}
}

/**
 * Creates and initializes an event flags group and returns a pointer to the
 * kernel's data structure used to maintain that flag group. Must be called
 * exactly once for each event group, typically done in main() in user code.
 * @param initialValue gives the initial value the flags group will have.
 *   1 = event set, 0 = event not set
 */
YKEVENT* YKEventCreate(unsigned initialValue) {
    YKEVENT* tmp1;
    YKEnterMutex();
    // Take first unused EVENT from the array
    if (availEventIndex == MAX_EVENTs) {
        printString("\n\n!!!!!! Error, created too many EvEnTs !!!!!\n\n");
    }
    else {
        tmp1 = &YKEVENTArray[availEventIndex];
        availEventIndex++;
    }
    
    // Set tmp1's properties
    tmp1->value = initialValue;
    tmp1->waitList = NULL;
    YKExitMutex();
    return tmp1;
}

/**
 * Tests the value of the given event flags group against the mask and mode.
 * If the conditions are met, the function returns immediately. Otherwise
 * the calling task is blocked and the scheduler is called.
 * Wait Modes: EVENT_WAIT_ALL = task blocks until all bits in mask = flags
 *             EVENT_WAIT_ANY = block until any of the mask bits are set.
 * This function is only called by tasks, never by ISRs or handlers.
 * @param event the pointer to the flag group
 * @param eventMask the bits to test the flag group against
 * @param waitMode whether to wait for all or any
 * @return The value of the event flags group at the time the function returns
 */
unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode) {
	TCBptr tmp1, tmp2;
	u16 tempValue;
	YKEnterMutex();
	if ((waitMode == EVENT_WAIT_ALL) &&
	    ((eventMask & event->value) == eventMask)) { //don't block
        YKExitMutex();
        return event->value;
	}
	else if ((waitMode == EVENT_WAIT_ANY) && 
	        ((event->value) & eventMask)){ //don't block
		YKExitMutex();
		return event->value;
	}
	else { //block       
        //Remove from the Ready List and place on the event's blocked list
        YKCurrentTask->state = BLOCKED;
        YKCurrentTask->delayTicks = 0;  //doesn't matter    
        YKCurrentTask->eventMask = eventMask;
        YKCurrentTask->waitMode = waitMode;
        
        tmp1 = YKRdyList;		/* get ptr to TCB to change */
        YKRdyList = tmp1->next;	/* remove from ready list */
        tmp1->next->prev = NULL;	/* ready list is never empty */
        
        tmp2 = event->waitList;
        
        if (event->waitList == NULL) {
            event->waitList = tmp1;
            tmp1->next = NULL;
            tmp1->prev = NULL;
        }
        else if (tmp2->priority > tmp1->priority){
            // If there the head is lower priority
            tmp1->next = tmp2;
            tmp2->prev = tmp1;
            tmp1->prev = NULL;
            event->waitList = tmp1;        
        }
        // For all the other insertions, insert sorted by priority
        else {            
            // Moves tmp2 to item to insert below
            while (tmp2->priority < tmp1->priority) {
                if (tmp2->next != NULL) {
                    //If the following item is lower priority
                    if (tmp2->next->priority > tmp1->priority) {
                        break;
                    }
                    else {
                        tmp2 = tmp2->next;
                    }                    
                }
                else {
                    break;  //jump out of the loop
                }
            }
            if (tmp2->next == NULL) {
                tmp2->next = tmp1;
                tmp1->prev = tmp2;
                tmp1->next = NULL;
            }
            else {
                tmp2->next->prev = tmp1;
                tmp1->next = tmp2->next;
                tmp2->next = tmp1;
                tmp1->prev = tmp2;                
            }
        }
	    YKCurrentTask = tmp1;
	    if (runEnabled == 1) {
            YKScheduler();
        }
        YKExitMutex();		
        return event->value;	
	}	
}

/**
 * Similar to posting. It causes all the bits set in eventmask to be set in
 * the event flags group. Any tasks waiting on this group will have their wait
 * conditions checked against the new value of the flags. If conditions are met
 * then the task will be made ready. This can be called from both task code
 * and interrupt handlers. Calls the scheduler if tasks were made ready.
 * @param event the pointer to the flag group
 * @param eventMask the bits to set in the flags group
 */
void YKEventSet(YKEVENT* event, unsigned eventMask) {
	TCBptr tmp1, tmp2, itemToRemove, nextItem;
	YKEnterMutex();
	event->value = (event->value | eventMask);  // Only set bits in eventMask
	
	// Iterate over all tasks in waitList and move to ready if condition met.
	tmp1 = event->waitList;
	
	while (tmp1 != NULL) {
	    itemToRemove = NULL;
	    nextItem = tmp1->next;
	    if (tmp1->waitMode == EVENT_WAIT_ALL) {
	        if (((tmp1->eventMask & event->value) == tmp1->eventMask)) {
	            // Update event->waitlist
                if (tmp1 == event->waitList) { // if head of list
                    event->waitList = tmp1->next;
                    event->waitList->prev = NULL;                    
                }
                else if (tmp1->next == NULL) { // tail of list
                    tmp1->prev->next = NULL;
                }
                else {                
                    // Remove from waitlist
                    tmp1->next->prev = tmp1->prev;
                    tmp1->prev->next = tmp1->next;
                }
                
                //Flag to be transfered to ReadyList
                itemToRemove = tmp1;               
	        }
	    }
	    else if (tmp1->waitMode == EVENT_WAIT_ANY) {
	        if ((event->value) & eventMask) {        
	            // Update event->waitlist
                if (tmp1 == event->waitList) { // if head of list
                    event->waitList = tmp1->next;
                    event->waitList->prev = NULL;                    
                }
                else if (tmp1->next == NULL) { // tail of list
                    tmp1->prev->next = NULL;
                }
                else {                
                    // Remove from waitlist
                    tmp1->next->prev = tmp1->prev;
                    tmp1->prev->next = tmp1->next;
                }
	            itemToRemove = tmp1; 
	        }
	    }	
	    
	    if (itemToRemove != NULL) {
	        // Update itemToRemove's properties
	        itemToRemove->state = READY;

	        tmp2 = YKRdyList; /* put in ready list (idle task always at end) */
            while (tmp2->priority < itemToRemove->priority) {
                tmp2 = tmp2->next;
            }
            if (tmp2->prev == NULL)	{/* insert before TCB pointed to by tmp2 */
                YKRdyList = tmp1;
            }
            else {
                tmp2->prev->next = tmp1;
            }
            itemToRemove->prev = tmp2->prev;            
            itemToRemove->next = tmp2;
            tmp2->prev = itemToRemove;	    
	    }
	    
        // Advance to the next element if there is one
        tmp1 = nextItem;
	}	
    // Only call the scheduler if running from task code
    if (YKISRDepth == 0) {
        YKCurrentTask->state = PAUSED;
        YKScheduler();
    }
	YKExitMutex();
}

/**
 * Simply causes all the bits that are set in the eventmask to be reset to 0
 * in the given event flags group. Does not unblock tasks or call the scheduler.
 * Can be called from both task and interrupt code.
 * @param event the pointer to the flag group
 * @param eventMask the bits to set in the flags group
 */
void YKEventReset(YKEVENT* event, unsigned eventMask) {
	YKEnterMutex();
	event->value = ((~eventMask) & event->value);
	YKExitMutex();
}


#ifdef DEBUG
/**
 * Debugging function used to print out the current contents of the lists
 */
void dumpList(void) {
    TCBptr tmp, tmp3;
    int i;
    int head, tail;
    void* baseAddress;
    struct msg * tmpMsg;
    YKEnterMutex();
    printString("\n\r==========================\n\r");
    printString("Dumping the Current Lists:\n\r\n\r");
    printString("Current Task: 0x");
    printWord((u16) YKCurrentTask);
    printString(" \n\r");
    printString("    [");
	printInt(YKCurrentTask->priority);
	printString(", 0x");
	printWord((u16)YKCurrentTask->sp);
	printString(", ");
	printInt(YKCurrentTask->state);
	printString(", ");
	printInt(YKCurrentTask->delayTicks);
	printString("]\n\r\n\r");
	
    printString("Suspended List: \n\r");
    tmp = YKSuspList;
    tmp3 = YKSuspList;
    while (tmp != NULL) {     // Iterate through all suspended tasks
  	    printString("    [");
	    printInt(tmp->priority);
	    printString(", 0x");
	    printWord((u16)tmp->sp);
	    printString(", ");
	    printInt(tmp->state);
	    printString(", ");
	    printInt(tmp->delayTicks);
	    printString("]\n\r");
        // Advance to the next element if there is one
        tmp = tmp3->next;
        tmp3 = tmp;
    }
    printString("\n\r");
    printString("Ready List: \n\r");  
    tmp = YKRdyList;
    tmp3 = YKRdyList;
    while (tmp != NULL) {     // Iterate through all suspended tasks  
        printString("    [");
        printInt(tmp->priority);
	    printString(", 0x");
	    printWord((u16)tmp->sp);
        printString(", ");
        printInt(tmp->state);
        printString(", ");
        printInt(tmp->delayTicks);
        printString("]\n\r");

        // Advance to the next element if there is one
        tmp = tmp3->next;
        tmp3 = tmp;
    }
    
    printString("\n\rYKISRDepth: ");
    printInt(YKISRDepth);
    printString("\n\r");
    
    printString("Semaphore Information: \n\r");
    for (i = 0; i < availSemIndex; i++) {
        printString("    Semaphore #");
        printInt(i);
        printString(" Value: ");
        printInt(YKSEMArray[i].value);
        printString("\n\r");
        printString("    Wait List: \n\r");
        tmp = YKSEMArray[i].waitList;
        tmp3 = YKSEMArray[i].waitList;
        while (tmp != NULL) {
      	    printString("        [");
	        printInt(tmp->priority);
	        printString(", 0x");
	        printWord((u16)tmp->sp);
	        printString(", ");
	        printInt(tmp->state);
	        printString(", ");
	        printInt(tmp->delayTicks);
	        printString("]\n\r");
            // Advance to the next element if there is one
            tmp = tmp3->next;
            tmp3 = tmp;
        }
        printString("\n\r");
    }
    printString("\n\r");
    
    //TODO(lukehsiao@byu.edu) this is specifically hard-coded for Lab 7
    printString("Event Information: \n\r");
    for (i = 0; i < availEventIndex; i++) {
        printString("    Event #");
        printInt(i);
        printString(" Value: ");
        printWord(YKEVENTArray[i].value);
        printString("\n\r");
        
        printString("    Wait List: \n\r");
        tmp = YKEVENTArray[i].waitList;
        tmp3 = YKEVENTArray[i].waitList;
        while (tmp != NULL) {
      	    printString("        [");
	        printInt(tmp->priority);
	        printString(", 0x");
	        printWord((u16)tmp->sp);
	        printString(", ");
	        printWord(tmp->eventMask);
	        printString(", ");
	        printInt(tmp->delayTicks);
	        printString("]\n\r");
            // Advance to the next element if there is one
            tmp = tmp3->next;
            tmp3 = tmp;
        }
        printString("\n\r");
     
    }
    
    printString("==========================\n\r\n\r");
    YKExitMutex();
}
#endif


