/*****************************************************************
// Interrupt Handlers to by called by the ISR
// ECEn 425 | Lab 3
// Luke Hsiao
// 19 Sept 2014
*****************************************************************/
#include "clib.h"
#include "yakk.h"
#include "lab7defs.h"

extern int KeyBuffer;


void keyboard_handler() {
    char c;
    c = KeyBuffer;

    if(c == 'a') YKEventSet(charEvent, EVENT_A_KEY);
    else if(c == 'b') YKEventSet(charEvent, EVENT_B_KEY);
    else if(c == 'c') YKEventSet(charEvent, EVENT_C_KEY);
    else if(c == 'd') YKEventSet(charEvent, EVENT_A_KEY | EVENT_B_KEY | EVENT_C_KEY);
    else if(c == '1') YKEventSet(numEvent, EVENT_1_KEY);
    else if(c == '2') YKEventSet(numEvent, EVENT_2_KEY);
    else if(c == '3') YKEventSet(numEvent, EVENT_3_KEY);
    else if(c == 'l') dumpList();  // for debugging
    else {
        print("\nKEYPRESS (", 11);
        printChar(c);
        print(") IGNORED\n", 10);
    }
}

void tick_handler(void)
{
    //printString("TICK ");
    //printInt(YKTickNum);
    //printNewLine();
}	

void reset_handler() {
    dumpList();
    exit(0);
}



