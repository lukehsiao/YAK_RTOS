/********************************************************************
 * Luke Hsiao & Tyler Bellows
 * ECEn 425 | Lab 4A
 * 30 Sept 2014
 * 
 * This file is for kernel code, not modified by the user.  It
 * outlines the declarations for TCB, YKSEM, and YKQ as well as
 * prototypes for kernel functions.  Globals that are shared with
 * application code should be declared extern in this file.
 *******************************************************************/

#define u16 unsigned int
#define u8 unsigned char
#define RUNNING 0
#define READY 1
#define BLOCKED 2
#define INTERRUPTED 3
#define PAUSED 4
#define NULL 0
#define FLAG_DEFAULT 0x0200
#define SAVE_CONTEXT 1
#define DONT_SAVE_CONTEXT 0
#define EVENT_SET 1
#define EVENT_WAIT_ALL 8
#define EVENT_WAIT_ANY 9


#include "yaku.h"

typedef struct taskblock* TCBptr;
// Definition of our TCB
typedef struct taskblock {
    void* sp;       // Stack Pointer for this task
    u8 priority;    // Unique priority of the task (0-100).  Also doubles as ID.
    u8 state;       // State of the task. 0 = running, 1 = ready, 
                    // 2 = blocked, 3 = interrupted, 4 = paused
    u16 delayTicks; // number of ticks to delay
    u16 eventMask;   // Event mask this task is waiting for
    u8  waitMode;    // Wait for ALL or ANY
    TCBptr prev;       // Pointer to the previous TCB in the linked list
    TCBptr next;       // Pointer to the next TCB in the linked list
} TCB;

// Each semaphore contains a value and a list of tasks waiting on it.
typedef struct semaphore* SEMptr;
typedef struct semaphore {
    TCBptr waitList;    // Pointer to the head of a list waiting to run.
    int value;           // Actual value of the semaphore.
} YKSEM;

//Definition of our Queue
typedef struct YKQ* YKQptr;
typedef struct queue {
    u16 length;
    u16 head; //= 0;
    u16 tail; //= 0;
    void** baseAddress;
    TCBptr waitList;    // Pointer to the head of a list waiting to run.
} YKQ;

//Definition of our Event
typedef struct YKEVENT* YKEVENTptr;
typedef struct yak_event {
	TCBptr waitList; //Pointer to the head of a list waiting to run
	u16 value; //Actual value of the event
} YKEVENT;

  
//***************** Global Variables ********************************
extern u16 YKIdleCount;    // increments each loop of the idle task
extern u16 YKCtxSwCount;   // incremented each time a context switch occurs
extern u16 YKTickNum;      // incremented each time the kernel's tick handlerruns

//================End Global Variables ==============================


//***************** Assembly Kernel Functions************************
extern void YKEnterMutex(void);
extern void YKExitMutex(void);
extern void YKDispatcher(u16 saveContext);

//==================End Assembly Kernel Functions====================

/**
 * Initializes all required kernel data structures. This must be called
 * from main() in the application code before any other YAK functions.
 * It must create YKIdleTask and is responsible for allocating its
 * stack space according to a #define.
 *
 */
void YKInitialize(void);

/**
 * This is the kernel's idle task, which is always the lowest
 * priority task in the system. It just spins in while(1).
 * This is only referenced by kernel code and is never
 * explicitly called. 
 * It also increments the global variable YKIdleCount atomically.
 *
 */
void YKIdleTask(void);

/**
 * Registers a task with the kernel, causing the allocation and
 * initialization of whatever data structures the kernel uses to
 * represent tasks.  Must be called exactly once for every task
 * in the system. Note *taskStack should be one work BEYOND the top of
 * stack. This can never be run by an interrupt handler.
 *
 * @param (*task)(void) function pointer to the C function of task code
 * @param *taskStack pointer to the top of the stack space reserved for task
 * @param priority A priority value from [0,100]. Lower # = higher priority.
 *
 */
void YKNewTask(void(*task)(void), void *taskStack, u8 priority);

/**
 * Called from main() in the application code and never returns.
 * It tells the kernel to begin execution of the tasks in the application
 * code.  It marks the transition from initial setup to actual execution.
 * At least one user-defined task must be created before this is called.
 */
void YKRun(void);

/**
 * Delays a task for the specified number of clock ticks.  After 
 * saving context, etc, this calls the scheduler.  After the specified
 * number of ticks, the kernel marks the task as ready.
 * If called with a count of 0, it does not delay and simply returns.
 * This is only called by tasks, never by interrupt handlers or ISRs.
 */

/*
Initializes data structure used to maintain queue and returns pointer to it.
Called once per message queue (ideally in main). 
*/
YKQ *YKQCreate(void **start, u16 length);

/* 
Removes and returns the oldest message from the message queue if non-empty. 
If the message queue is empty, calling task is suspended by the kernel untila  message is placed in the queue.
Called only by tasks, NEVER by interrupt code.
*/
void *YKQPend(YKQ *queue);

/*
Inserts a message (pointer) into message queue.
queue = pointer to queue to use
msg = pointer to message
Returns 1 if the queue is not full and the message insertion was successful
Returns 0 if the queue is full and the message was not inserted.
If suspended tasks are qaiting for a message from this queue, the highest priority suspended task is made ready to run.
Called from both task and interrupt code. 
*/
int YKQPost(YKQ *queue, void *message);

void YKDelayTask(u16 count);

/**
 * Must be called from the tick ISR each time it runs. Responsible for
 * the bookkeeping required to support timely reawakening of delayed tasks
 * The tick ISR may also call a user tick handler if user code requires
 * actions to be taken on each clock tick.
 *
 */
void YKTickHandler(void);

/**
 * This function must be called near the beginning of each ISR just before
 * interrupts are re-enabled.  This simply increments YKISRDepth
 */
void YKEnterISR(void);

/**
 * This function must be called near the beginning of each ISR just before
 * interrupts are re-enabled.  This simply increments YKISRDepth
 */
void YKExitISR(void);

/**
 * Determines the highest priority task, then calls the dispatcher
 * to cause that task to execute.
 *
 */
void YKScheduler(void);

/**
 * This function creates and initializes a semaphore and must be called exactly
 * once per semaphore. This call is typically in main in the user code. The
 * initialization value must be non-negative. The value returned is a pointer
 * to the data structure used by the kernel to represent the semaphore.
 * YKSEM is a typedef defined in a kernel header file that must be included
 * in any user file that uses semaphores.  Despite having a pointer to this
 * struct, user code never needs to know implementation details of semaphores.
 */
YKSEM* YKSemCreate(int initialValue);

/**
 * This function tests the value of the indicated semaphore then decrements it.
 * If the value before decrementing was greater than zero, the code returns to
 * the caller. If the value before decrementing was less than or equal to zero,
 * the calling task is suspended by the kernel until the semaphore is available
 * and the scheduler is called. This function is called only by tasks, never by
 * ISRs or interrupt handlers.
 */
void YKSemPend(YKSEM* semaphore);

/**
 * This function increments the value of the indicated semaphore. If any
 * suspended tasks are waiting for this semaphore, the waiting task with the
 * highest priority is made ready.  Unlike YKSemPend, this function may be
 * called from both task code and interrupt handlers. If called from task code
 * then the function should call the scheduler. Otherwise, the scheduler isn't
 * called.
 */
void YKSemPost(YKSEM* semaphore);

/**
 * Creates and initializes an event flags group and returns a pointer to the
 * kernel's data structure used to maintain that flag group. Must be called
 * exactly once for each event group, typically done in main() in user code.
 * @param initialValue gives the initial value the flags group will have.
 *   1 = event set, 0 = event not set
 */
YKEVENT* YKEventCreate(unsigned initialValue);

/**
 * Tests the value of the given event flags group against the mask and mode.
 * If the conditions are met, the function returns immediately. Otherwise
 * the calling task is blocked and the scheduler is called.
 * Wait Modes: EVENT_WAIT_ALL = task blocks until all bits in mask = flags
 *             EVENT_WAIT_ANY = block until any of the mask bits are set.
 * This function is only called by tasks, never by ISRs or handlers.
 * @param event the pointer to the flag group
 * @param eventMask the bits to test the flag group against
 * @param waitMode whether to wait for all or any
 * @return The value of the event flags group at the time the function returns
 */
unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode);

/**
 * Similar to posting. It causes all the bits set in eventmask to be set in
 * the event flags group. Any tasks waiting on this group will have their wait
 * conditions checked against the new value of the flags. If conditions are met
 * then the task will be made ready. This can be called from both task code
 * and interrupt handlers. Calls the scheduler if tasks were made ready.
 * @param event the pointer to the flag group
 * @param eventMask the bits to set in the flags group
 */
void YKEventSet(YKEVENT* event, unsigned eventMask);

/**
 * Simply causes all the bits that are set in the eventmask to be reset to 0
 * in the given event flags group. Does not unblock tasks or call the scheduler.
 * Can be called from both task and interrupt code.
 * @param event the pointer to the flag group
 * @param eventMask the bits to set in the flags group
 */
void YKEventReset(YKEVENT* event, unsigned eventMask);


/**
 * Debugging function used to print out the current contents of the lists
 */
void dumpList(void);
