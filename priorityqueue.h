/********************************************************************
 * Luke Hsiao & Tyler Bellows
 * ECEn 425 | RTOS
 * 19 Nov 2014
 * 
 * This contains functionality for priority queues
 *******************************************************************/
 
// Call before using a queue
void InitializePriorityQueue(PriorityQueue* queue);

TCBptr PriorityQueueRemove(TCBptr itemToRemove, TCBptr headOfList);

void PriorityQueueAdd(TCBptr itemToAdd, TCBptr headOfList);
