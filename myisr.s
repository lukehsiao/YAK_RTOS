;********************************************************************
; YAK Interrupt Service Routines
; ECEn 425 | Lab 4
; Luke Hsiao & Tyler Bellows
; 1 Oct 2014
;
; 1.Save the context of whatever was running by pushing all registers 
;   onto the stack, except SP, SS, CS, IP, and the flags.
; 2.Enable interrupts to allow higher priority IRQs to interrupt.
; 3.Run the interrupt handler, which is usually a C function that 
;   does the work for this interrupt.
; 4.Disable interrupts.
; 5.Send the EOI command to the PIC, informing it that the handler 
;   is finished.
; 6.Restore the context of what was running before the interrupt
;   occurred by popping the registers saved in step 1 off the stack.
; 7.Execute the iret instruction. This restores in one atomic 
;   operation the values for IP, CS, and the flags, which were 
;   automatically saved on the stack by the processor when the 
;   interrupt occurred. This effectively restores execution to the 
;   point where the interrupt occurred.
;********************************************************************

tick_isr:
    ; Save context
    push	ax		    
	push	bx
	push	cx
	push	dx
	push	si
	push	di
	push	bp
	push	es
	push	ds
    
    cmp     word[YKISRDepth], 0     ; If we're interrupting task code
    jne     tick_2
    mov     bx, [YKCurrentTask]     ; get what YKCurrentTask Points to
    mov     [bx], sp                ; Store SP in the TCB    
tick_2:    
    call    YKEnterISR
    sti                 ; enable interrupts
    call    YKTickHandler 
    call    tick_handler    ; addition for lab 6
    cli                 ; disable interrupts
    
    ; Acknowledge Interrupt was handled
    mov     al, 0x20    ; load nonspecific eoi value
    out     0x20, al    ; write eoi to pic
    
    call    YKExitISR
    
    
tick_3:         ;if returning to interrupt code    
    pop     ds
    pop     es
    pop     bp
    pop     di
    pop     si
    pop     dx
    pop     cx
    pop     bx
    pop     ax
    iret


keyboard_isr:
    ; Save context
    push	ax		    
	push	bx
	push	cx
	push	dx
	push	si
	push	di
	push	bp
	push	es
	push	ds
	
    cmp     word[YKISRDepth], 0
    jne     keyboard_2
    mov     bx, [YKCurrentTask]     ; get what YKCurrentTask Points to
    mov     [bx], sp                ; Store SP in the TCB     
keyboard_2:    
    call    YKEnterISR
    sti                 ; enable interrupts
    call    keyboard_handler
    cli                 ; disable interrupts
    
    ; Acknowledge Interrupt was handled
    mov     al, 0x20    ; load nonspecific eoi value
    out     0x20, al    ; write eoi to pic

    call    YKExitISR
keyboard_3: ;if returning to interrupt code
    pop     ds
    pop     es
    pop     bp
    pop     di
    pop     si
    pop     dx
    pop     cx
    pop     bx
    pop     ax
    iret
    
reset_isr:
    call reset_handler
