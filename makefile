#################################################################
# ECEn 425 | Lab 4
# @author Luke Hsiao
# @author Tyler Bellows
# @date 2 Oct 2014
#################################################################
PP = cpp
CC = c86
CFLAGS = -g

lab7.bin: lab7final.s
	nasm lab7final.s -o lab7.bin -l lab7.lst

lab7final.s: clib.s myisr.s myinth.s yaks.s yakc.s lab7app.s
	cat clib.s myisr.s myinth.s yaks.s yakc.s lab7app.s > lab7final.s

lab7app.s: lab7app.c
	$(PP) lab7app.c lab7app.i
	$(CC) $(CFLAGS) lab7app.i lab7app.s
	
myinth.s: myinth.c
	$(PP) myinth.c myinth.i
	$(CC) $(CFLAGS) myinth.i myinth.s
	
yakc.s: yakc.c
	$(PP) yakc.c yakc.i
	$(CC) $(CFLAGS) yakc.i yakc.s

clean:
	rm -f lab7.bin lab7.lst lab7final.s yakc.s yakc.i lab7app.i
	rm -f Emu86OutputPipe_lhsiao lab7app.s
	rm -f myinth.i myinth.s

