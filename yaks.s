;********************************************************************
; Luke Hsiao & Tyler Bellows
; ECEn 425 | Lab 4A
; 30 Sept 2014
; 
; This file is for kernel code routines written in assembly.
;
;*******************************************************************

global YKEnterMutex     ; make this visible to the C code
global YKExitMutex      ; make this visible to the C code
global YKDispatcher     ; make this visible to the C code
global saveContext    
global restoreContext 

; This function simply disables interrupts.  It may be called from
; application or system code.  
YKEnterMutex:
    cli             ; disable interrupts
    ret
    

; This function simply enables interrupts.  It may be called from
; application or system code.  
YKExitMutex:
    sti             ; enable interrupts
    ret
    
; Causes the execution of the task identified by the scheduler. The task
; may be running for the first time, or resuming execution.
; Called directly only in kernel code and never in user code.
YKDispatcher:
    ; Check the parameter
    push    bp
    mov     bp, sp
    cmp     word[bp+4], 0
    pop     bp          ;restore BP
    je      dispatcher_2
    
    ; If we need to save context:
    push    cs          ; save CS in correct spot
    push    ax          ; dummy spot for IP
    push	ax		    ; save context
	push	bx
	push	cx
	push	dx
	push	si
	push	di
	push	bp
	push	es
	push	ds	
	mov     bx, [YKCurrentTask]      ;ax = Current Task
	mov     [bx], sp              ;YKSuspList->sp = sp
	
	;fix IP
	push    bp
	mov     bp, sp
	mov     bx, [bp+24]
	mov     [bp+20], bx
	
	;fix flags
	pushf
	pop     bx              ; save flags in bx
	or      bx, 0x200       ; reenable interrupts
	mov     [bp+24], bx
	pop     bp
 
 dispatcher_2:   
    mov     bx, word[YKRdyList]
    mov     word[YKCurrentTask], bx
    mov     byte[bx+3], 0               ;YKCurrentTask->state = 0 (RUNNING)
    mov     sp,  [bx]               ;sp = YKCurrentTask->sp
	pop	    ds		    ;restore context
	pop	    es
	pop	    bp
	pop	    di
	pop	    si
	pop	    dx
	pop	    cx
	pop	    bx
	pop	    ax    
	iret
        
